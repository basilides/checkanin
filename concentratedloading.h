#ifndef CONCENTRATEDLOADING_H
#define CONCENTRATEDLOADING_H

#include <host.h>

class ConcentratedLoading
{
public:

    ConcentratedLoading();
    ConcentratedLoading(int h, double p);
    ConcentratedLoading(int h, double p, const QVector<Host>& hosts);
    bool operator ==(const ConcentratedLoading&) const;

    int getHost() const;
    double getPower() const;
    double getPos() const;

private:

    int host;
    double power;
    double pos;
};

#endif // CONCENTRATEDLOADING_H
