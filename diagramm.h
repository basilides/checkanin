#ifndef DIAGRAMM_H
#define DIAGRAMM_H

#include <QWidget>
#include <mainwindow.h>
#include <Geometry.h>

class MainWindow;
class Construction;

class Diagramm : public QWidget
{
    Q_OBJECT
public:
    explicit Diagramm(QWidget *parent = 0, MainWindow *window = 0, const Construction* c = 0, int state = 0);
    
public:
    void setState(int s);

protected:
    void paintEvent(QPaintEvent *);

private:

    MainWindow* w;
    QWidget* parent;
    QPainter* painter;

private:
    int state;
    int max_p;
    const Construction* constr;
    int width, heigth;

    void paintN();
    void paintU();

    void drawGreed(const QString &t);
    void setScales();

    double w_scale, h_scale;
    qreal sx, sy;

    int y_c;

    static const int free_space = 50;
};

#endif // DIAGRAMM_H
