#include "constructionpainter.h"

constructionPainter::constructionPainter(QWidget *parent, MainWindow *window, Construction *c) :
    QWidget(parent), w(window),
    constr(c),
    sx(1.0), sy(1.0),
    width(720), heigth(420)
{
    setCenter();
    resize(width, heigth);
}

void constructionPainter::drawDouble(double x, int x_pos, int y_pos, int x_size, int y_size)
{
    QString a; a.setNum(x); painter->drawText(x_pos, y_pos, x_size, y_size, 0, a);
}

void constructionPainter::setCenter()
{
    center.x = width/2;
    center.y = heigth/2;
}

void constructionPainter::paint()
{
    repaint();
}

void constructionPainter::setScales()
{
    width_scale = (double)(width - free_space_x*2)/(constr->getWidth()*2);
    height_scale = (double)(heigth - free_space_y*2)/(constr->getHigh());
}

void constructionPainter::drawRect(double x, double y, double w)
{
    painter->setPen(QPen(Qt::black, 3, Qt::SolidLine));

    painter->drawLine(x, center.y + w/2, y, center.y + w/2);
    painter->drawLine(x, center.y - w/2, y, center.y - w/2);
    painter->drawLine(x, center.y + w/2, x, center.y - w/2);
    painter->drawLine(y, center.y + w/2, y, center.y - w/2);
}

void constructionPainter::paintEvent(QPaintEvent *)
{
    constr->calculate();
    painter = new QPainter(this);
    painter->scale(sx, sy);
    setScales();
    drawCoordinate();
    drawHosts();
    drawRods();
    drawConcentrLoadings();
    drawRunLoadings();
}


void constructionPainter::drawHosts()
{
    foreach (const Host & elem, constr->getHosts()) {
        if(elem.getBearing()) painter->setPen(QPen(Qt::red, 10, Qt::SolidLine));
        else painter->setPen(QPen(Qt::green, 10, Qt::SolidLine));
        painter->drawPoint(getScreenCoordinateX(elem.getPos()), center.y);
    }

}

void constructionPainter::drawRunLoadings()
{
    painter->setPen(QPen(Qt::darkGreen, 1, Qt::SolidLine));
    int size_arrow_w = 20;
    int size_arrow_h = 10;
    int arrows_q = 4; //

    foreach (const RunningLoading & rl, constr->getRunningLoadings()) {

        double x_1 = getScreenCoordinateX(rl.getPosBegin());
        double x_2 = getScreenCoordinateX(rl.getPosEnd());

        if(x_1 > x_2) std::swap(x_1, x_2); // если балки соединяются справа налево; нам для рисунка нужно x_1 < x_2

        //double w = getScreenWidth(rl.getWidth())/2;

        Direction arrow_dir = RIGHT;

        if(rl.getPower() < 0) arrow_dir = LEFT;

        painter->drawLine(x_1, center.y, x_2, center.y); // сама синяя фигня, которая потом будет обводиться стрелочками

        int _size = x_2 - x_1 - size_arrow_w;

        for(int i = 0; i <= arrows_q; i++)
            drawArrow(x_1 + size_arrow_w + _size/arrows_q*i, center.y, arrow_dir, size_arrow_w, size_arrow_h);

        drawDouble(rl.getPower(), x_1 + _size/2, center.y - 50, 50, 20);
    }
}

void constructionPainter::drawConcentrLoadings()
{
    foreach (const ConcentratedLoading cf, constr->getConcentratedLoadings()) {
        int x_ = getScreenCoordinateX(cf.getPos());
        painter->setPen(QPen(Qt::blue, 2, Qt::SolidLine));

        const int value_size_x = 50; const int value_size_y = 30;
        int space_for_arrow = 25;
        int size_arrow = 60;

        Direction arrow_dir = RIGHT;

        if(cf.getPower() < 0) // положительная нагрузка будет рисоваться слева
        {
            space_for_arrow *= -1;
            size_arrow *= -1;
            arrow_dir = LEFT;
        }

        painter->drawLine(x_ + size_arrow, center.y, x_, center.y);
        drawArrow(x_ + size_arrow, center.y, arrow_dir);
        drawDouble(cf.getPower(), x_ + space_for_arrow, center.y + space_for_arrow, value_size_x, value_size_y);

    }
}

void constructionPainter::drawRods()
{
    painter->setPen(QPen(Qt::black, 3, Qt::SolidLine));

    foreach (const Rod & r, constr->getRods())
    {
        // теперь рисуем балку
        drawRect(getScreenCoordinateX(r.getPosBegin()),
                 getScreenCoordinateX(r.getPosEnd()),
                 getScreenWidth(r.getA()));
    }
}

//void constructionPainter::wheelEvent(QWheelEvent *e)
//{
//    if(e->delta()>0) {
//        sx += 0.1; sy += 0.1;
//    } else {
//        sx -= 0.1; sy -= 0.1;
//    }
//    repaint();
//}

int constructionPainter::getScreenCoordinateX(double constrCoordX)
{
    return center.x + width_scale*constrCoordX;
}

int constructionPainter::getScreenWidth(double w)
{
    return height_scale*w;
}

void constructionPainter::drawCoordinate()
{
    drawCoordinateGreed();
    drawCoordinateAxis();
    drawCoordinateSticks();
}

void constructionPainter::drawCoordinateGreed()
{
    painter->setPen(QPen(Qt::gray, 0.5, Qt::SolidLine));

    int greed_size = 10;

    for(int j = 0; j <= heigth/greed_size/2; j++) // все горизонтальные
    {
        painter->drawLine(0, center.y - j*greed_size, width, center.y - j*greed_size);
        painter->drawLine(0, center.y + j*greed_size, width, center.y + j*greed_size);
    }

    for(int i = 0; i <= width/greed_size/2; i++) // вертикальные
    {
        painter->drawLine(center.x - i*greed_size, 0, center.x - i*greed_size, heigth);
        painter->drawLine(center.x + i*greed_size, 0, center.x + i*greed_size, heigth);
    }


    painter->setPen(QPen(Qt::black, 4, Qt::SolidLine));
    painter->drawPoint(center.x, center.y);
}

void constructionPainter::drawCoordinateSticks()
{
    const double stick_space = 50;
    const double cost = stick_space/width_scale;
    const int stick_number = width/stick_space/2;
    painter->setPen(QPen(Qt::black, 2, Qt::SolidLine));
    const int stick_size = 8;
    for(int i = 0; i <= stick_number; i++)
    {
        painter->drawLine(center.x + i*stick_space, center.y - stick_size,
                          center.x + i*stick_space, center.y + stick_size);// рисуем засечку в положительную сторону
        drawDouble(cost*i, center.x + i*stick_space, center.y, 32, 20);

        painter->drawLine(center.x - i*stick_space, center.y - stick_size,
                          center.x - i*stick_space, center.y + stick_size);// в отрицательную сторону
        drawDouble(cost*i*-1, center.x - i*stick_space, center.y, 40,40);
    }
}

void constructionPainter::drawCoordinateAxis()
{
    painter->setPen(QPen(Qt::black, 2, Qt::SolidLine));

    painter->drawLine(0, center.y, width, center.y);
    painter->drawLine(center.x, 0, center.x, heigth);

    drawArrow(width, center.y, RIGHT);
    drawArrow(center.x, 0, UP);

    const int X_size = 50;
    const int Y_size = 50;

    painter->drawText(width - X_size/2, center.y + X_size/2, X_size, Y_size, 0, "X");
    painter->drawText(center.x + X_size/2, 0, X_size, Y_size, 0, "Y");
}

void constructionPainter::drawArrow(double x, double y, Direction d, int a_w, int a_h)
{
    switch(d)
    {
    case LEFT:
    {
        painter->drawLine(x + a_w, y + a_h/2, x, y);
        painter->drawLine(x + a_w, y - a_h/2, x, y);
        break;
    }
    case UP:
    {
        painter->drawLine(x - a_w/2, y + a_h, x, y);
        painter->drawLine(x + a_w/2, y + a_h, x, y);
        break;
    }
    case RIGHT:
    {
        painter->drawLine(x - a_w, y + a_h/2, x, y);
        painter->drawLine(x - a_w, y - a_h/2, x, y);
        break;
    }
    case DOWN:
    {
        painter->drawLine(x - a_w/2, y - a_h, x, y);
        painter->drawLine(x + a_w/2, y - a_h, x, y);
        break;
    }
    }
}
