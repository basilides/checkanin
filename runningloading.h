#ifndef RUNNINGLOADING_H
#define RUNNINGLOADING_H

#include <rod.h>

class RunningLoading
{
public:
    RunningLoading();
    RunningLoading(int r, double p);

    RunningLoading(int r, double p, const QVector<Rod>& rods);

    bool operator ==(const RunningLoading& rl) const;

    int getRod() const;
    double getPower() const;
    double getPosBegin() const;
    double getPosEnd() const;
    double getWidth() const;

private:
    int rod;
    double power;
    double pos_1;
    double pos_2;
    double width;
};

#endif // RUNNINGLOADING_H
