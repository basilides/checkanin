#include "construction.h"
#include <algorithm>
#include <functional>
#include <iostream>

template<class T> void delete_(QVector<T>& v, std::function<bool (const T&)> predicate)
{
    std::for_each(v.begin(), v.end(),
                  [predicate, &v] (const T& elem) mutable
    {
                  if(predicate(elem))
    {
                  v.erase(v.begin() + v.indexOf(elem));
}
});
}

template<class T> void removeEquals(QVector<T>& v, const T& eq)
{
    delete_<T>(v,
               [eq] (const T& elem) -> bool
    {
               return elem == eq;
});
}

Construction::Construction()
{

}

double Construction::getHigh() const
{
    return (*std::max_element(rods.begin(), rods.end(),
                              [] (Rod A, Rod B)
    {
                              return A < B;
})).getA();
}

double Construction::getWidth() const
{
    if(hosts.isEmpty()) return 1;

    return qAbs((*std::max_element(hosts.begin(), hosts.end(),
                                  [] (Host A, Host B) -> bool
    {
                                  return qAbs(A.getPos()) < qAbs(B.getPos());
})).getPos());
}

void Construction::addHost(double x, bool a)
{
    hosts.append(Host(x, a));
}


void Construction::addRod(int beg, int end, double a, double e, double p)
{
    rods.append(Rod(beg, end, a, e, p, hosts));
}

void Construction::addConcentratedLoading(int host, double power)
{
    ConcentratedLoadings.append(ConcentratedLoading(host, power, hosts));
    hosts[host].setCForce(power);

}

void Construction::addRunningLoading(int rod, double power)
{
    RunningLoadings.append(RunningLoading(rod, power, rods));
    rods[rod].setRForce(power);
}

void Construction::deleteHost(double x, bool a)
{
    removeEquals<Host>(hosts, Host(x,a));
}

void Construction::deleteRod(int beg, int end, double a, double e, double p)
{
    removeEquals<Rod>(rods, Rod(beg, end, a, e, p));
}

void Construction::deleteConcentratedLoading(int host, double power)
{
    removeEquals<ConcentratedLoading>(ConcentratedLoadings, ConcentratedLoading(host, power));
}

void Construction::deleteRunningLoading(int rod, double power)
{
    removeEquals<RunningLoading>(RunningLoadings, RunningLoading(rod, power));
}

QVector<Host> Construction::getHosts() const
{
    return hosts;
}

QVector<Rod> Construction::getRods() const
{
    return rods;
}

QVector<ConcentratedLoading> Construction::getConcentratedLoadings() const
{
    return ConcentratedLoadings;
}

QVector<RunningLoading> Construction::getRunningLoadings() const
{
    return RunningLoadings;
}

void Construction::straight(int n, double **a)
{
    double v;
    for(int k = 0; k < n-1; k++)
    {
        int im = k;
        for(int i = k+1; i < n; i++)
            if(qAbs(a[im][k]) < qAbs(a[i][k])) im = i;
        if(im != k)
        {
            for(int j = 0; j < n; j++)
            {
                v = a[im][j];
                a[im][j] = a[k][j];
                a[k][j] = v;
            }
            v = d[im];
            d[im] = d[k];
            d[k] = v;
        }

        for(int i = k+1; i < n; i++)
        {
            v = 1.0*a[i][k]/a[k][k];
            a[i][k] = 0;
            d[i] = d[i] - v*d[k];
            if(v != 0)
                for(int j = k+1; j < n; j++)
                    a[i][j] = a[i][j] - v*a[k][j];
        }
    }
}

void Construction::back(int n, double **a)
{
    m[n-1] = 1.0*d[n-1]/a[n-1][n-1];
    for(int i = n-2; i >= 0; i--)
    {
        double s = 0;
        for(int j = i+1; j < n; j++)
        {
            s = s + a[i][j]*m[j];
        }
        m[i] = 1.0*(d[i] - s)/a[i][i];
    }
}

void Construction::solveGauss(int n, double **a)
{
    straight(n, a);
    back(n, a);
}

void Construction::calculate()
{
    if(correctionCheck()){
        int s = hosts.size();
        b.resize(s);
        a.resize(s-1); c.resize(s-1);
        d.resize(s); m.resize(s);

        b[0] = rods[0].getReaction();
        b[s-1] = rods[s-2].getReaction();

        for(int i = 1; i < s-1; i++) // заполняем b
        {
            b[i] = rods[i-1].getReaction() + rods[i].getReaction();
        }

        for(int i = 0; i < s-1; i++)
        {
            a[i] = rods[i].getReaction() * (-1);
        }

        c = a;

        d[0] = hosts[0].getCForce() + rods[0].getQ();
        d[s-1] = hosts[s-1].getCForce() + rods[s-2].getQ();

        for(int i = 1; i < s-1; i++)
        {
            d[i] = rods[i-1].getQ() + rods[i].getQ() + hosts[i].getCForce();
        }

        for(int i = 0; i < s; i++)
        {
            if(hosts[i].getBearing())
            {
                d[i] = 0; // меняем местами
                if(i == 0) // обнуляем коэффициенты
                {
                    a[i] = 0;

                }
                else if(i == s-1)
                {
                    c[i-1] = 0;
                }

                else
                {
                    a[i] = 0;
                    c[i-1] = 0;
                }
            }
        }

        QString _s;

        foreach (double a1, a) {
            _s.setNum(a1); qDebug(_s.toAscii() + "A");
        }


        foreach (double b1, b) {
            _s.setNum(b1); qDebug(_s.toAscii() + "B");
        }


        foreach (double c1, a) {
            _s.setNum(c1); qDebug(_s.toAscii() + "C");
        }



        // наполняем матрицу
        double **matrix = new double *[s];
        for(int i = 0; i < s; i++)
            matrix[i] = new double[s];



        for(int i = 0; i < s; i++)
            for(int j = 0; j < s; j++)
            {
                matrix[i][j] = 0;
                if(i == j) matrix[i][j] = b[i];
                if(j == i + 1) matrix[i][j] = c[i];
                if(j == i - 1) matrix[i][j] = a[j];
            }

        for(int i = 0; i < s; i++)
        {
            for(int j = 0; j < s; j++)
            {
                std::cout << matrix[i][j] << " ";
            }
            std::cout << std::endl;
        }


        solveGauss(s, matrix);

        for(int i = 0; i < s; i++)
            if(hosts[i].getBearing()) m[i] = 0;

        for(int i = 0; i < s; i++)
        {
            _s.setNum(m[i]); qDebug(_s.toAscii());
        }
    }
}

bool Construction::correctionCheck()
{
    return hosts.size() == rods.size()+1 && rods.size() != 0;
}

double Construction::getS(int p, double x) const
{
    return getN(p, x)/rods[p].getA();
}

double Construction::getU(int p, double x) const
{
    return (m[p] + x/rods[p].getSize()*(m[p+1] - m[p]) +
            rods[p].getRForce()*rods[p].getSize()*rods[p].getSize()*x)/
            (2*rods[p].getE()*rods[p].getA()*rods[p].getSize())*
            (1 - x/rods[p].getSize());
}

double Construction::getN(int p, double x) const
{
    return rods[p].getReaction()*(m[p+1] - m[p]) + rods[p].getQ()*(1 - 2*x/rods[p].getSize());
}

double Construction::getMaxN() const
{
    double result = 0;
    for(int i = 0; i < rods.size(); i++)
    {
        if(qAbs(getN(i, 0)) > result) result = qAbs(getN(i,0));
    }
    return result;
}

double Construction::getMaxU() const
{
    double result = 0;
    for(int i = 0; i < rods.size(); i++)
    {
        if(qAbs(getU(i, 0)) > result) result = qAbs(getN(i,0));
    }
    return result;
}

double Construction::getMaxS() const
{
    double result = 0;
    for(int i = 0; i < rods.size(); i++)
    {
        if(qAbs(getS(i, 0)) > result) result = qAbs(getN(i,0));
    }
    return result;
}

bool Construction::fail() const
{
    double m_s = getMaxS();
    for(int i = 0; i < rods.size(); i++)
    {
        if(rods[i].getPosStress() < m_s) return true;
    }
    return false;
}
