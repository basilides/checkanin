#include "diagramm.h"
#include <iostream>

Diagramm::Diagramm(QWidget *parent, MainWindow *window, const Construction* c, int s)
    :QWidget(parent), w(window), state(s), constr(c),
      width(560), heigth(280),
      sx(.0), sy(.0)
{
    resize(width, heigth);
    max_p = c->getRods().size();
    y_c = (heigth-free_space)/2;
}

void Diagramm::paintEvent(QPaintEvent *)
{
    painter = new QPainter(this);
    switch(state)
    {
    case 0:
        break;
    case 1:
    {
        paintN();
       break;
    }
    case 2:
    {
        paintU();
        break;
    }
    }
}

void Diagramm::setState(int s)
{
    state = s;
}

void Diagramm::paintN()
{
    drawGreed("N");
    setScales();

    double y_ = 0;
    int p = 0;
    double goal = constr->getRods()[p].getSize();

    QString _s;
    double x_ = 0;

    for(int i = free_space; i < width - free_space; i++, x_++)
    {
        if(i/w_scale > goal)
        {
            if(!p == constr->getRods().size() - 1) // безопасность для плавающей точки я хз как она себя поведет
            {
                p++;
                goal += constr->getRods()[p].getSize();

            }
            // поставить засечку
            painter->drawLine(i, y_c+10, i, y_c-10 );

            _s.setNum(y_);
            painter->drawText(i, y_c + 20, 50,20, 0, _s);

            x_ = 0;
        }

        y_ = constr->getN(p, x_/w_scale);

        // нарисовать по иксу игрик
        painter->drawPoint(i, y_c - y_*h_scale);
        std::cout << "y_ , p, y*h_scale, w_scale, h_scale" << y_ << " " << p << " " << y_*h_scale << " " <<
                     w_scale << " " << h_scale << " " << std::endl;
    }

}

void Diagramm::paintU()
{
    drawGreed("U");
    setScales();

    double y_ = 0;
    int p = 0;
    double goal = constr->getRods()[p].getSize();

    QString _s;

    for(int i = free_space; i < width - free_space; i++)
    {
        if(i/w_scale > goal)
        {
            if(!p == constr->getRods().size() - 1) // безопасность для плавающей точки я хз как она себя поведет
            {
                p++;
                goal += constr->getRods()[p].getSize();

            }
            // поставить засечку
            painter->drawLine(i, y_c+10, i, y_c-10 );
            _s.setNum(y_);
            painter->drawText(i, y_c + 20, 20,20, 0, _s);
        }

        if(y_ == width - free_space - 1)
        {
            painter->drawLine(i-20, y_c+10, i, y_c-10 );
            _s.setNum(y_);
            painter->drawText(i-20, y_c + 20, 20,20, 0, _s);
        }

        y_ = constr->getU(p, i/w_scale);

        // нарисовать по иксу игрик
        painter->drawPoint(i, y_c - y_*h_scale);
        std::cout << "y_ , p, y*h_scale, w_scale, h_scale" << y_ << " " << p << " " << y_*h_scale << " " <<
                     w_scale << " " << h_scale << " " << std::endl;
    }
}

void Diagramm::setScales()
{
    w_scale = (width - free_space) / constr->getWidth();

    if(state == 1) h_scale = (heigth - free_space) / constr->getMaxN()/2;
    if(state == 2) h_scale = (heigth - free_space) / constr->getMaxU();
}

void Diagramm::drawGreed(const QString& t)
{
    painter->drawLine(free_space, free_space, free_space, heigth - free_space);
    painter->drawLine(free_space, heigth - free_space - y_c, width - free_space, heigth - free_space - y_c);

    int a_w = 30; int a_h = 30;

    int x = free_space; int y = free_space;
    painter->drawLine(x - a_w/2, y + a_h, x, y);
    painter->drawLine(x + a_w/2, y + a_h, x, y);

    x = width - free_space; y = heigth - free_space - y_c;
    painter->drawLine(x - a_w, y + a_h/2, x, y);
    painter->drawLine(x - a_w, y - a_h/2, x, y);

    painter->drawText(width - free_space - 20, heigth - free_space - 40 - y_c, 20, 20, 0, "X");
    painter->drawText(free_space - 20, free_space - 20, 20, 20, 0, t);

}
