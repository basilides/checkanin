#ifndef HOST_H
#define HOST_H

#include <QVector>

class Host
{
public:
    Host();
    Host(double x, bool a);

    bool operator ==(const Host& h) const;

    double getPos() const;
    bool getBearing() const;

    void setCForce(double c_f);
    double getCForce() const;
private:
    double pos;
    bool bearing;
    double c_force;

};
#endif // HOST_H
