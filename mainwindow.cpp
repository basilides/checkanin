#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    cp = new constructionPainter(ui->construction_paint_field, this, &constr);
    di = new Diagramm(ui->diag_paint_field, this, &constr);
}


void MainWindow::paintEvent(QPaintEvent *)
{
    drawRods();
    drawHosts();
    drawConcentratedLoadings();
    drawRunningLoadings();
}

void MainWindow::drawHosts()
{
    ui->textBrowser->clear();
    int n_ = 0;
    QString n, h, b;

    ui->textBrowser->append("{#, COORDINATE, TYPE}");

    foreach (const Host & a, constr.getHosts()) {
        if(a.getBearing()) b = "bearing"; else b = "host";
        h.setNum(a.getPos()); n.setNum(n_ ++);
        ui->textBrowser->append(n + "    " + h + "     " + b);
    }
}

void MainWindow::drawRods()
{
    ui->textBrowser_2->clear();
    int n_ = 0;
    QString n, h_b, h_e, a, e;

    ui->textBrowser_2->append("{#, HOST_BEG_#, HOST_END_#, A, E}");

    foreach (const Rod & r, constr.getRods()) {
        n.setNum(n_ ++); h_b.setNum(r.getHostBegin()); h_e.setNum(r.getHostEnd()); a.setNum(r.getA()); e.setNum(r.getE());
         ui->textBrowser_2->append(n + "    " + h_b + "    " + h_e + "    " + a + "    " + e);
    }

}

void MainWindow::drawConcentratedLoadings()
{
    ui->textBrowser_3->clear();
    int n_ = 0;
    QString n, h, p;

    ui->textBrowser_3->append("{#, HOST_#, VALUE}");

    foreach (const ConcentratedLoading & l, constr.getConcentratedLoadings()) {
        n.setNum(n_++); h.setNum(l.getHost()); p.setNum(l.getPower());
        ui->textBrowser_3->append(n + "    " + h + "    " + p);
    }

}

void MainWindow::drawRunningLoadings()
{
    ui->textBrowser_4->clear();
    int n_ = 0;
    QString n, r, p;

    ui->textBrowser_4->append("{#, ROD_#, VALUE}");

    foreach (const RunningLoading & l, constr.getRunningLoadings()) {
        n.setNum(n_ ++); r.setNum(l.getRod()); p.setNum(l.getPower());
        ui->textBrowser_4->append(n + "    " + r + "    " + p);
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked() // узлы
{
    constr.addHost(ui->lineEdit->text().toDouble(), ui->checkBox->isChecked());
    repaint();
}

void MainWindow::on_pushButton_2_clicked() // балки
{
    constr.addRod(ui->lineEdit_2->text().toInt(), ui->lineEdit_3->text().toInt(),
                  ui->lineEdit_9->text().toDouble(), ui->lineEdit_8->text().toDouble(),
                  ui->lineEdit_10->text().toDouble());
    repaint();
}

void MainWindow::on_pushButton_3_clicked() // сосред. нагр.
{
    constr.addConcentratedLoading(ui->lineEdit_4->text().toInt(), ui->lineEdit_5->text().toDouble());
    repaint();
}

void MainWindow::on_pushButton_4_clicked() // распред. нагр.
{
    constr.addRunningLoading(ui->lineEdit_6->text().toInt(), ui->lineEdit_7->text().toDouble());
    repaint();
}

void MainWindow::on_pushButton_8_clicked()
{
    constr.deleteHost(ui->lineEdit->text().toDouble(), ui->checkBox->isChecked());
    repaint();
}

void MainWindow::on_pushButton_7_clicked()
{
    constr.deleteRod(ui->lineEdit_2->text().toInt(), ui->lineEdit_3->text().toInt(),
                     ui->lineEdit_9->text().toDouble(), ui->lineEdit_8->text().toDouble(),
                     ui->lineEdit_10->text().toDouble());
    repaint();
}

void MainWindow::on_pushButton_6_clicked()
{
    constr.deleteConcentratedLoading(ui->lineEdit_4->text().toInt(), ui->lineEdit_5->text().toDouble());
    repaint();
}

void MainWindow::on_pushButton_5_clicked()
{
    constr.deleteRunningLoading(ui->lineEdit_6->text().toInt(), ui->lineEdit_7->text().toDouble());
    repaint();
}

void MainWindow::on_pushButton_9_clicked()
{
    // 11, 13, 12
    QString _s; _s.setNum(constr.getN(ui->lineEdit_11->text().toInt(), ui->lineEdit_13->text().toDouble()));
    ui->lineEdit_12->setText(_s);
}


void MainWindow::on_pushButton_10_clicked()
{
    // 14, 15, 16
    QString _s; _s.setNum(constr.getU(ui->lineEdit_14->text().toInt(), ui->lineEdit_15->text().toDouble()));
    ui->lineEdit_16->setText(_s);
}

void MainWindow::on_pushButton_11_clicked()
{
    // 17, 19
    QString _s; _s.setNum(constr.getS(ui->lineEdit_17->text().toInt(), ui->lineEdit_18->text().toDouble()));
    ui->lineEdit_19->setText(_s);
}

void MainWindow::on_pushButton_12_clicked()
{
    di->setState(1);
    di->repaint();
}

void MainWindow::on_pushButton_13_clicked()
{
    di->setState(2);
    di->repaint();
}

void MainWindow::on_pushButton_14_clicked()
{
    QString _s; if(constr.fail()) _s = "yes"; else _s = "no";
    ui->lineEdit_20->setText(_s);
}
