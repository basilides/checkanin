#ifndef CONSTRUCTION_H
#define CONSTRUCTION_H

#include <QQueue>
#include <QList>
#include <QString>
#include <Geometry.h>
#include <deque>
#include <concentratedloading.h>
#include <runningloading.h>

class Construction
{
public:
    Construction();

    bool correctionCheck();

    void addRod(int beg, int end, double a, double e, double p);
    void addHost(double x, bool a);

    void addConcentratedLoading(int host, double power);
    void addRunningLoading(int rod, double power);

    void deleteRod(int beg, int end, double a, double e, double p);
    void deleteHost(double x, bool a);

    void deleteConcentratedLoading(int host, double power);
    void deleteRunningLoading(int rod, double power);

    QVector<Host> getHosts() const;
    QVector<Rod> getRods() const;

    QVector<ConcentratedLoading> getConcentratedLoadings() const;
    QVector<RunningLoading> getRunningLoadings() const;

    double getWidth() const;
    double getHigh() const;

    void calculate();

    double getU(int, double) const;
    double getN(int, double) const;
    double getS(int, double) const;

    double getMaxU() const;
    double getMaxN() const;
    double getMaxS() const;
    bool fail() const;

private:
    QVector<Rod> rods;
    QVector<Host> hosts;
    QVector<ConcentratedLoading> ConcentratedLoadings;
    QVector<RunningLoading> RunningLoadings;

    QVector<double> u;
    QVector<double> N;
    QVector<double> s;

    QVector<double> A;
    QVector<double> P;
    QVector<double> Q;
    QVector<double> a; // три диагонали матрицы
    QVector<double> b;
    QVector<double> c;
    QVector<double> m; // дельта
    QVector<double> d; // b


    void solveGauss(int, double**);
    void straight(int, double**);
    void back(int, double**);

    void resetRodsNLoadings();
};

#endif // CONSTRUCTION_H
