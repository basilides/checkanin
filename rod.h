#ifndef ROD_H
#define ROD_H

#include <host.h>

class Rod
{
public:
    Rod();
    Rod(int beg, int end, double a, double e, double p, const QVector<Host>& hosts);
    Rod(int beg, int end, double a, double e, double p);

    bool operator <(const Rod& r) const;
    bool operator ==(const Rod& r) const;

    int getHostBegin() const;
    int getHostEnd() const;
    double getA() const;
    double getE() const;
    double getPosBegin() const;
    double getPosEnd() const;
    double getPosStress() const;

    double getReaction() const;
    double getSize() const;
    double getQ() const;

    void setRForce(double r_f);
    double getRForce() const;
private:

    int host_b;
    int host_e;
    double pos_1;
    double pos_2;
    double A;
    double E;
    double r_force;
    double posStress;
};

#endif // ROD_H
