#include "host.h"

Host::Host()
{
}

Host::Host(double x, bool a)
    : bearing(a), pos(x), c_force(0)
{
}

bool Host::operator ==(const Host& h) const
{
    return pos == h.pos && bearing == h.bearing;
}

double Host::getPos() const
{
    return pos;
}

bool Host::getBearing() const
{
    return bearing;
}

void Host::setCForce(double c_f)
{
    c_force = c_f;
}

double Host::getCForce() const
{
    return c_force;
}
