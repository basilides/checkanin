#ifndef CONSTRUCTIONPAINTER_H
#define CONSTRUCTIONPAINTER_H

#include <QWidget>
#include <mainwindow.h>
#include <QWheelEvent>

#include <Geometry.h>

class MainWindow;
class Construction;

class constructionPainter : public QWidget
{
    Q_OBJECT
public:
    constructionPainter(QWidget *parent = 0, MainWindow *window = 0, Construction *c = 0);



    void paint();

private:
    int width, heigth;
    static const int free_space_x = 50; // свободное место для отрисовки сил
    static const int free_space_y = 100;
    qreal sx, sy;

    double width_scale, height_scale; // точек экрана занимает один метр

    MainWindow* w;
    QWidget* parent;
    QPainter* painter;

protected:
    void paintEvent(QPaintEvent *);
   // void wheelEvent(QWheelEvent *e);

private:
    Construction *constr;

    void drawCoordinate(); void drawCoordinateAxis(); void drawCoordinateGreed(); void drawCoordinateSticks();
    void drawRods();
    void drawHosts();
    void drawRunLoadings();
    void drawConcentrLoadings();

    void setScales();
    void setCenter();

    void drawRect(double, double, double);
    void drawArrow(double, double, Direction, int a_w = 30, int a_h = 30);
    void drawDouble(double, int, int, int, int);

    int getScreenCoordinateX(double constrCoordX); // по координате в конструкции определяет точку на экране
    int getScreenWidth(double w);

    Point center;


};

#endif // CONSTRUCTIONPAINTER_H
