#ifndef GEOMETRY_H
#define GEOMETRY_H

struct Point
{
    int x;
    int y;
};

enum Direction{LEFT, RIGHT, UP, DOWN};

#endif // GEOMETRY_H
