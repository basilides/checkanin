#-------------------------------------------------
#
# Project created by QtCreator 2013-11-26T23:10:03
#
#-------------------------------------------------

QT       += core gui

TARGET = Resistance
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    construction.cpp \
    constructionpainter.cpp \
    host.cpp \
    rod.cpp \
    concentratedloading.cpp \
    runningloading.cpp \
    diagramm.cpp

HEADERS  += mainwindow.h \
    construction.h \
    constructionpainter.h \
    Geometry.h \
    host.h \
    rod.h \
    concentratedloading.h \
    runningloading.h \
    diagramm.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++0x
