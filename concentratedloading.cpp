#include "concentratedloading.h"

ConcentratedLoading::ConcentratedLoading()
{
}

ConcentratedLoading::ConcentratedLoading(int h, double p)
    :host(h), power(p)
{
}

ConcentratedLoading::ConcentratedLoading(int h, double p, const QVector<Host> &hosts)
    : host(h), power(p),
      pos(hosts[host].getPos())
{
}

bool ConcentratedLoading::operator ==(const ConcentratedLoading& cl) const
{
    return host == cl.host && power == cl.power;
}

int ConcentratedLoading::getHost() const
{
    return host;
}

double ConcentratedLoading::getPower() const
{
    return power;
}

double ConcentratedLoading::getPos() const
{
    return pos;
}
