#include "runningloading.h"

RunningLoading::RunningLoading()
{
}

RunningLoading::RunningLoading(int r, double p)
    : rod(r), power(p)
{
}

RunningLoading::RunningLoading(int r, double p, const QVector<Rod>& rods)
    : rod(r), power(p),
      pos_1(rods[r].getPosBegin()),
      pos_2(rods[r].getPosEnd()),
      width(rods[r].getA())
{
}

bool RunningLoading::operator ==(const RunningLoading& rl) const
{
    return rod == rl.rod && power == rl.power;
}

int RunningLoading::getRod() const
{
    return rod;
}

double RunningLoading::getPower() const
{
    return power;
}

double RunningLoading::getPosBegin() const
{
    return pos_1;
}

double RunningLoading::getPosEnd() const
{
    return pos_2;
}

double RunningLoading::getWidth() const
{
    return width;
}
