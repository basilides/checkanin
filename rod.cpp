#include "rod.h"
#include <QString>

Rod::Rod()
{
}

Rod:: Rod(int beg, int end, double a, double e, double p, const QVector<Host>& hosts)
    : host_b(beg), host_e(end), A(a), E(e), r_force(0),
      pos_1(hosts[beg].getPos()), pos_2(hosts[end].getPos()), posStress(p)
{
    //QString a_; a_.setNum(pos_1); qDebug(a_.toAscii()); a_.setNum(pos_2); qDebug(a_.toAscii());
}

Rod::Rod(int beg, int end, double a, double e, double p)
    : host_b(beg), host_e(end), A(a), E(e), r_force(0), posStress(p)
{
}

bool Rod::operator <(const Rod& r) const
{
    return A < r.A;
}

bool Rod::operator ==(const Rod& r) const
{
    return host_b == r.host_b && host_e == r.host_e && A == r.A && E == r.E;
}

int Rod::getHostBegin() const
{
    return host_b;
}

int Rod::getHostEnd() const
{
    return host_e;
}

double Rod::getA() const
{
    return A;
}

double Rod::getE() const
{
    return E;
}

double Rod::getPosBegin() const
{
    return pos_1;
}

double Rod::getPosEnd() const
{
    return pos_2;
}

double Rod::getReaction() const
{
    return E*A/getSize();
}

double Rod::getQ() const
{
    return r_force*getSize()/2;
}

double Rod::getSize() const
{
    return qAbs(pos_2 - pos_1);
}

void Rod::setRForce(double r_f)
{
    r_force = r_f;
}

double Rod::getRForce() const
{
    return r_force;
}

double Rod::getPosStress() const
{
    return posStress;
}
